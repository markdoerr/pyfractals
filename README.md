# pyfractals

fractals with python

## Installation

python 2.7

virtualenv ~/pyfractal27

### jupyter in venv

pip install ipykernel
ipython kernel install --user --name=pyfractal27

### dependancies for the fractals

pip install numpy scipy matplotlib turtle

